FROM telkomindonesia/alpine-openjre-tomcat:3.5-8.0-8.5-novol
MAINTAINER Ibnu Mas'ud <ibnu.masud@telkom.co.id>
RUN apk update
RUN apk add tzdata
RUN export TZ='Asia/Jakarta'
RUN apk add msttcorefonts-installer
RUN apk add ttf-dejavu
COPY . /opt/tomcat/webapps